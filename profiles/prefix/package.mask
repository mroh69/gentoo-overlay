>app-misc/elasticsearch-5.6.16
# because of dev-libs/flatbuffers-9999::lmiphay
>dev-libs/flatbuffers-1.11.0
>=dev-lang/ruby-2.7
# bug 704090 (8.06.2 has this also)
#>dev-ml/labltk-8.06.2
>=dev-lang/python-3.9.0_alpha2
