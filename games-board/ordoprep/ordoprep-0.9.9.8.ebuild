# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Transform, filter, and compact pgn files to be used by Ordo."
HOMEPAGE=" https://github.com/michiguel/Ordoprep"

SRC_URI="https://github.com/michiguel/Ordoprep/archive/v${PVR}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~arm64 ~arm ~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

S="${WORKDIR}/Ordoprep-${PVR}"

PATCHES=("${FILESDIR}"/Makefile.patch)

src_prepare() {
	default
}

src_install() {
	dobin ordoprep
	dodoc README.md
}
