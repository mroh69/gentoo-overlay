# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Rating for chess engines, players, or other games"
HOMEPAGE=" https://github.com/michiguel/Ordo"

SRC_URI="https://github.com/michiguel/Ordo/archive/v${PVR}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~arm64 ~arm ~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

S="${WORKDIR}/Ordo-${PV}"

PATCHES=("${FILESDIR}"/Makefile.patch)

src_prepare() {
	default
}

src_install() {
	dobin ordo
	dodoc README.md
}
