# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson git-r3

DESCRIPTION="Lc0 is a UCI-compliant chess engine designed to play chess via neural network."
HOMEPAGE="https://github.com/LeelaChessZero/lc0"
SRC_URI=""https://github.com/LeelaChessZero/lc0/archive/v${PVR}.tar.gz

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="opencl -openblas"

DEPEND="sys-libs/zlib:0= dev-libs/protobuf:0=
	opencl? ( virtual/opencl )
	openblas? ( sci-libs/openblas )"
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

# work around the git clone inside the meson build that doesnt work because of sandboxing

EGIT_CLONE_TYPE=shallow

src_unpack() {
	default
	git-r3_fetch "https://github.com/LeelaChessZero/lczero-common.git"
	git-r3_checkout "https://github.com/LeelaChessZero/lczero-common.git" "${S}/libs/lczero-common/"
}

src_configure() {
	local emesonargs=(
		-Dprotobuf_include=/usr/include/google/protobuf
		-Dgtest=false
		$(meson_use opencl)
		$(meson_use openblas)
	)
	meson_src_configure
}
