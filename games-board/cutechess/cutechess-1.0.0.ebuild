# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit qmake-utils desktop xdg-utils

DESCRIPTION="Cute Chess is a graphical user interface for playing chess."
HOMEPAGE="https://github.com/cutechess/cutechess"

SRC_URI="https://github.com/cutechess/cutechess/archive/gui-1.0.0.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}/cutechess-gui-1.0.0"

DEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5
	dev-qt/qtconcurrent:5
"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

src_prepare() {
	default
}

src_configure() {
	eqmake5
}

src_install() {
	dobin projects/cli/cutechess-cli projects/gui/cutechess
	dodoc README.md
	doman docs/cutechess-cli.6 docs/engines.json.5
	newicon -s 32 projects/gui/res/icons/cutechess_32x32.xpm cutechess.xpm
	domenu dist/linux/cutechess.desktop
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
