# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs git-r3

DESCRIPTION="C port of stockfish"
HOMEPAGE="https://github.com/syzygy1/Cfish"

EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~arm64 ~arm ~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE="-numa -pgo armv7 cpu_flags_x86_avx2 cpu_flags_x86_popcnt cpu_flags_x86_sse debug +optimize general-32 general-64"

DEPEND="numa? ( sys-process/numactl )"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

S="${WORKDIR}/cfish-9999/src"

src_prepare() {
	default
}

src_compile() {
	local my_arch

	# generic unoptimized first
	use general-32 && my_arch=general-32
	use general-64 && my_arch=general-64

	# x86
	use x86 && my_arch=x86-32-old
	use cpu_flags_x86_sse && my_arch=x86-32

	# amd64
	use amd64 && my_arch=x86-64
	use cpu_flags_x86_popcnt && my_arch=x86-64-modern

	# both bmi2 and avx2 are part of hni (haswell new instructions)
	use cpu_flags_x86_avx2 && my_arch=x86-64-bmi2

	# other architectures
	use armv7 && my_arch=armv7
	use ppc && my_arch=ppc
	use ppc64 && my_arch=ppc64

	emake $(usex pgo "profile-build" "build") ARCH="${my_arch}" \
			COMPCC=$(tc-getCC) \
			numa=$(usex numa "yes" "no") \
			debug=$(usex debug "yes" "no") \
			optimize=$(usex optimize "yes" "no")
}

src_install() {
	dobin cfish
	dodoc ../README.md
}
